# API Documentation for News MS
## Version: 1.0



**Contact information:**  
Smart Campus 2nd Group  
roufiq@s.itb.ac.id  
---

* [Language](#language)
* [List of API services](#list-of-api-services)
* [Built With](#built-with)
* [Authors](#authors)
* [License](#license)
* [Acknowledgments](#acknowledgments)

---

## Language

This services are written in **PHP**

## List of API Services
### /agenda

#### GET
##### Summary:

Get All Published and Verified Agenda Detail registered in ITB

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | Working |
| default | an "unexpected" error |

#### POST
##### Summary:

Store the item of Agenda

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| body | body | The Agenda to create | No |  |
| u | query | user token from SSO identity manager | No | string |

##### Responses

| Code | Description |
| ---- | ----------- |
| 201 | Success |
| default | an unexpected error |

### /agenda/draft

#### GET
##### Summary:

Get All Draft Agenda Detail registered in ITB

##### Description:

in order to gain access, token must be as admin (0) or PR(3)

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| u | query | Token SSO from IdentityManagement | Yes | string |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | Working |
| 401 | Error: Unauthorized |
| default | an "unexpected" error |

### /agenda/verify/{id}

#### POST
##### Summary:

Verify the item of Agenda

##### Description:

in order to gain access the token must be admin role (0) or PR(3)

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| body | body | The Agenda to create | No |  |
| id | path | id agenda status draft | No | string |
| u | query | user token from SSO identity manager | No | string |

##### Responses

| Code | Description |
| ---- | ----------- |
| 201 | Success |
| default | an unexpected error |

### /agenda/detail/{id}/

#### GET
##### Summary:

Get one agenda detail from specified ID

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| id | path | ID agenda | Yes | string |
| u | query | Token SSO | Yes | string |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | Returns some faculty web address |
| 400 | Error: Bad request. When required parameters were not supplied. |

### /register

#### POST
##### Summary:

Register User

##### Description:

deprecated, see identitymanager to get access

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| User | body | User to create | No |  |

##### Responses

| Code | Description | Schema |
| ---- | ----------- | ------ |
| 201 | Success |  |
| default | an "unexpected" error | [User](#user) |

### /login

#### POST
##### Summary:

Login User

##### Description:

deprecated, see identitymanager to get access

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| User | body | User to create | No |  |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | Success! |
| 403 | The Username or Password not Valid! |

### /news

#### GET
##### Summary:

Get All Published News from ITB web

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | Working |
| default | an "unexpected" error |

#### POST
##### Summary:

Create News Article include tags

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| body | body | The News Article  to create | No |  |
| u | query | user token from SSO identity manager | No | string |

##### Responses

| Code | Description |
| ---- | ----------- |
| 201 | Success |
| default | an unexpected error |

### /news/draft

#### GET
##### Summary:

Get All Draft Article News in ITB

##### Description:

in order to gain access, token must be as admin (0) or PR(3)

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| u | query | Token SSO from IdentityManagement | Yes | string |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | Working |
| 401 | Error: Unauthorized |
| default | an "unexpected" error |

### /news/detail/{id}/

#### GET
##### Summary:

Get one news detail from specified ID

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| id | path | ID agenda | Yes | string |
| u | query | Token SSO | Yes | string |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | Returns some faculty web address |
| 400 | Error: Bad request. When required parameters were not supplied. |

### /news/{id}/

#### DELETE
##### Summary:

Delete one news detail from specified ID

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| id | path | ID agenda | Yes | string |
| u | query | Token SSO | Yes | string |

##### Responses

| Code | Description |
| ---- | ----------- |
| 201 | Resource Updated |
| 400 | Error: Bad request. When required parameters were not supplied. |

### /faculties

#### GET
##### Summary:

Get Faculties Detail registered in ITB

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| u | query | Token SSO from IdentityManagement | Yes | string |

##### Responses

| Code | Description | Schema |
| ---- | ----------- | ------ |
| 200 | Working |  |
| default | an "unexpected" error | [Fakultas](#fakultas) |

#### POST
##### Summary:

Store the detail of Faculties

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| body | body | The faculty to create | No |  |
| u | query | user token from SSO identity manager | No | string |

##### Responses

| Code | Description |
| ---- | ----------- |
| 201 | Sukses |
| default | an unexpected error |

### /faculties/{id}/

#### GET
##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| id | path | ID Fakultas | Yes | string |
| u | query | Token SSO | Yes | string |

##### Responses

| Code | Description | Schema |
| ---- | ----------- | ------ |
| 200 | Returns some faculty web address | [Fakultas](#fakultas) |
| 400 | Error: Bad request. When required parameters were not supplied. |  |

### /scrap

#### GET
##### Summary:

get news title and link from all faculties in ITB

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| u | query | Token SSO from Identitymanagement | Yes | string |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | Returns some article from various web faculties in ITB |
| 500 | Internal Server Error. When required parameters were not supplied. |

### /scrap/{id}

#### GET
##### Summary:

get news title and link from certain faculties based ID in ITB

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| id | path | ID of web faculties in ITB | Yes | string |
| u | query | Token SSO from Identitymanagement | Yes | string |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | Returns some article from various web faculties in ITB |
| 500 | Internal Server Error. When required parameters were not supplied. |

### /scrap/{id}/detail/{id_post}

#### GET
##### Summary:

get full content news from web faculties based post id  in ITB

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| id | path | ID of web faculties in ITB | Yes | string |
| id_post | path | post ID from web | Yes | string |
| u | query | Token SSO from Identitymanagement | Yes | string |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | Returns some article from various web faculties in ITB |
| 500 | Internal Server Error. When required parameters were not supplied. |

### /scrap/{id}/flash/{num}

#### GET
##### Summary:

get flash news in certain faculties

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| id | path | ID of web faculties in ITB | Yes | string |
| u | query | Token SSO from Identitymanagement | Yes | string |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | Returns some article from various web faculties in ITB |
| 500 | Internal Server Error. When required parameters were not supplied. |

### /nearby

#### GET
##### Summary:

get flash news based user location ITB

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| lat | query | Latitude of position | Yes |  |
| long | query | Longitude of position | Yes |  |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | Returns some article from various web faculties in ITB |
| 500 | Internal Server Error. When required parameters were not supplied. |

### /jadwal

#### GET
##### Summary:

Get All Schedule Reporting in ITB

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | Working |
| default | an "unexpected" error |

#### POST
##### Summary:

Store the item of schedule

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| body | body | The Schedule to create | No |  |
| u | query | user token from SSO identity manager | No | string |

##### Responses

| Code | Description |
| ---- | ----------- |
| 201 | Success |
| default | an unexpected error |

### /jadwal/{id}/

#### GET
##### Summary:

Get one schedule detail from specified ID

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| id | path | ID kegiatan | Yes | string |
| u | query | Token SSO | Yes | string |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | Returns some faculty web address |
| 400 | Error: Bad request. When required parameters were not supplied. |

### /kegiatan

#### GET
##### Summary:

Get All Activities Reporting Agenda Detail registered in ITB

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | Working |
| default | an "unexpected" error |

#### POST
##### Summary:

Store the item of activities

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| body | body | The Agenda to create | No |  |
| u | query | user token from SSO identity manager | No | string |

##### Responses

| Code | Description |
| ---- | ----------- |
| 201 | Success |
| default | an unexpected error |

### /kegiatan/{id}/

#### GET
##### Summary:

Get one activities detail from specified ID

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| id | path | ID kegiatan | Yes | string |
| u | query | Token SSO | Yes | string |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | Returns some faculty web address |
| 400 | Error: Bad request. When required parameters were not supplied. |

### /comments

#### GET
##### Summary:

Get All Comments across all News from ITB web

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| u | query | user token from SSO identity manager | No | string |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | Working |
| default | an "unexpected" error |

#### POST
##### Summary:

Create News Comments include tags

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| body | body | The comments to create | No |  |
| u | query | user token from SSO identity manager | No | string |

##### Responses

| Code | Description |
| ---- | ----------- |
| 201 | Success |
| default | an unexpected error |

### /news/{id}/comments

#### GET
##### Summary:

Get list of commpents of specified ID news

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| id | path | ID news | Yes | string |
| u | query | Token SSO | Yes | string |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | Returns some faculty web address |
| 400 | Error: Bad request. When required parameters were not supplied. |

### /comments/hatespeech

#### GET
##### Summary:

get a list of comments suspected of being hatespeech

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| u | query | user token from SSO identity manager | No | string |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | Returns some faculty web address |
| 400 | Error: Bad request. When required parameters were not supplied. |

### Models


#### Agenda

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| id | integer |  | No |
| id_author | integer | userid who created agendas | No |
| id_verificator | integer | userid who created agendas | No |
| tanggal | string |  | No |
| organizer | string |  | No |
| category_event | string |  | No |
| partisipan | string |  | No |
| link_agenda | string |  | No |
| deskripsi | string |  | No |
| status | integer |  | No |
| created_at | string |  | No |
| updated_at | string |  | No |

#### Artikel

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| id | integer |  | No |
| id_fakultas | integer | ID Fakultas ITB | No |
| guid | string |  | No |
| modified_by | integer | userid | No |
| link | string | link content | No |
| title | string | title of article | No |
| content | string | content of article | No |
| status | integer | 0- draft<br>1- published | No |
| created_at | string |  | No |
| updated_at | string |  | No |

#### Berita

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| id | integer |  | No |
| author | integer | userid who created news article | No |
| modified_by | integer |  | No |
| date | string |  | No |
| title | string |  | No |
| content | string |  | No |
| status | integer | 0 - draft <br>1 - published | No |
| created_at | string |  | No |
| updated_at | string |  | No |

#### Fakultas

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| id | integer |  | No |
| nama_fakultas | string |  | No |
| nama_tag | string |  | No |
| status | boolean |  | No |
| created_at | string |  | No |
| updated_at | string |  | No |
| lat | string |  | No |
| long | string |  | No |

#### Jadwal

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| id | integer |  | No |
| id_kegiatan | integer |  | No |
| reporter | integer | userid reporter | No |
| tanggal | string |  | No |
| status | string | 0 - Dijadwalkan <br>1 - Selesai<br>2 - Ditunda | No |
| created_at | string |  | No |
| updated_at | string |  | No |

#### Kegiatan

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| id | integer |  | No |
| nama_kegiatan | string |  | No |
| topik | string |  | No |
| narasumber | string |  | No |
| deadline | string |  | No |
| status | integer | 0 - Belum assign jadwal <br>1 - Terjadwal | No |
| created_at | string |  | No |
| updated_at | string |  | No |

#### Komentar

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| id | integer |  | No |
| id_berita | integer |  | No |
| id_user | integer | userid dari pemberi komentar | No |
| comment | string | teks komentar | No |
| hate_speech | boolean | hasil identifikasi hatespeech | No |
| confidence | string | confidence hasil identifikasi hatespeech, rentang 0.0 sampai 1.0 | No |
| created_at | string |  | No |
| updated_at | string |  | No |

#### Peliputan

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| id | integer |  | No |
| id_jadwal | integer |  | No |
| hasil_peliputan | integer |  | No |
| kendala | string | kendala yang dialami reporter | No |
| created_at | string |  | No |
| updated_at | string |  | No |

#### Tag

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| id | integer |  | No |
| tag_name | string |  | No |
| created_at | string |  | No |
| updated_at | string |  | No |

#### User

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| id | integer |  | No |
| userid | string |  | No |
| email | string |  | No |
| password | string |  | No |
| api_token | string |  | No |
| role | integer | 0 - Admin<br>1- Mahasiswa<br>2- Dosen<br>3- Staf Teknis<br>4- Staf Akademik Kampus<br>5- Tamu<br>6- Dekan<br>7- Kaprodi<br>8- Staf Akademik Prodi | No |
## Built With

* [Lumen](https://lumen.laravel.com/) - The web framework used
* [Swagger](https://swagger.io/) - The API Documentation tools

## Authors

* **Moh. Roufiq Azmy    (23218027)** - [roufiq](http://178.128.104.74:9000/23218027)
* **You Ari Faeni       (23218046)** - [you](http://178.128.104.74:9000/23218046)
* **Hani Purwati H      (23218074)** - [hani](http://178.128.104.74:9000/23218074)

## License

This project is licensed under the MIT License

## Acknowledgments

* Prof. Suhardi as lecturer for ReSTI and PLTI course
* Mr. Novianto and Mr. Wardani as assistant for ReSTI and PLTI course
* Mr. Miswar, Mr. Alfa and Mr. Gery for assisting in this project
