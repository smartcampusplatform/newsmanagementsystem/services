<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $nama_kegiatan
 * @property string $topik
 * @property string $narasumber
 * @property string $deadline
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 * @property Jadwal $jadwalReporter
 */

/**
 *  @SWG\Definition()
 * Class Kegiatan
 * @package App
 */
class Kegiatan extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'kegiatan';

    /**
     * @var array
     */
    protected $fillable = ['nama_kegiatan', 'topik', 'narasumber', 'deadline', 'status'];

    protected $appends  = ['status_name'];

    public function getStatusNameAttribute($value)
    {
        $status_name= null;
        if($this->status==0)
            $status_name="Belum assign jadwal";
        elseif ($this->status==1)
            $status_name= "Terjadwal";
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function jadwalReporter()
    {
        return $this->hasOne('App\Jadwal', 'id_kegiatan');
    }

//    Swagger Property Definition Here:
    /**
     * @SWG\Property(property="id", type="integer", example=1)
     * @SWG\Property(property="nama_kegiatan", type="string", example="Peliputan Lomba" )
     * @SWG\Property(property="topik", type="string", example="berita")
     * @SWG\Property(property="narasumber", type="string", example="Prof. Dr. ABC")
     * @SWG\Property(property="deadline", type="string", example="2019-04-30")
     * @SWG\Property(property="status", type="integer", example=1, default=0, description="0 - Belum assign jadwal <br>1 - Terjadwal",minProperties=0, maxProperties=1)
     * @SWG\Property(property="created_at", type="string", example="2019-04-22 01:38:59")
     * @SWG\Property(property="updated_at", type="string", example="2019-04-22 01:38:59")
     */
}
