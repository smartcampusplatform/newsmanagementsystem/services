<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $tag_name
 * @property string $created_at
 * @property string $updated_at
 */

/**
 * @SWG\Definition()
 * Class Tag
 * @package App
 */
class Tag extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'tag';

    /**
     * @var array
     */
    protected $fillable = ['tag_name'];

    protected $hidden=['pivot'];

    /**
     * @param Builder $query
     * @param $nama
     * @return mixed
     */
    public function scopeOfTagName($query, $nama)
    {
        return $query->whereTagName($nama);

    }


    public function berita()
    {
        return $this->belongsToMany('App\Berita');
    }

    /**
     * @SWG\Property(property="id", type="integer", example=1)
     * @SWG\Property(property="tag_name", type="string", example="berita")
     * @SWG\Property(property="created_at", type="string", example="2019-04-22 01:38:59")
     * @SWG\Property(property="updated_at", type="string", example="2019-04-22 01:38:59")
     */
}
