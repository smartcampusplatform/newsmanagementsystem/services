<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $author
 * @property int $modified_by
 * @property string $date
 * @property string $title
 * @property string $content
 * @property integer $status
 * @property string $updated_at
 * @property string $created_at
 * @property User $user
 * @property User $editor
 * @property Komentar[] $komentars
 * @property Tag[] $tags
 */

/**
 *
 * @SWG\Definition(
 * )
 * Class Berita
 * @package App
 */
class Berita extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'berita';

    /**
     * @var array
     */
    protected $fillable = ['date', 'title', 'content', 'status','author', 'modified_by'];

    protected $hidden = ['author', 'modified_by','user','editor','status'];

    protected $appends =['author_name','editor_name','status_berita'];

//    protected $visible =['id','title','content','date','author_name','editor_name','created_at','updated_at','tags'];

    protected $with =['tags'];

//    protected $visible=['komentars'];

    /**
     * @param Builder $query
     */
    public function scopeDraft($query)
    {
        return $query->where('status','=',0);

    }
    /**
     * @param Builder $query
     */
    public function scopePublish($query)
    {
        return $query->where('status','=',1);

    }

    public function getStatusBeritaAttribute($value)
    {
        $status_berita=null;
        if($this->status==0)
        {
            $status_berita='draft';
        }
        elseif ($this->status==1)
            $status_berita='published';

        return $status_berita;
    }

    public function getAuthorNameAttribute($value)
    {
        $authorName=null;
        if($this->user){
            $authorName=$this->user->username;
        }
        return $authorName;
    }

    public function getEditorNameAttribute($value)
    {
        $editorName=null;
        if($this->editor){
            $editorName=$this->editor->username;
        }
        return $editorName;
    }

    public function getTagDetailAttribute($value)
    {
        $tags=[];
        if ($this->tags)
        {
            $tags=$this->tags;
        }
        return $tags;
    }



    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'author');
    }

    public function editor()
    {
        return $this->belongsTo('App\User', 'modified_by');
    }

    public function tags()
    {
        return $this->belongsToMany('App\Tag');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
//    public function tagBeritas()
//    {
//        return $this->hasMany('App\TagBerita', 'id_berita');
//    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function komentars()
    {
        return $this->hasMany('App\Komentar', 'id_berita');
    }

    /**
     * @SWG\Property(property="id", type="integer", example=1)
     * @SWG\Property(property="author", type="integer", example=1, description="userid who created news article")
     * @SWG\Property(property="modified_by", type="integer", example=2, default=null)
     * @SWG\Property(property="date", type="string", example="2019-04-30", default=null)
     * @SWG\Property(property="title", type="string", example="Judul Berita")
     * @SWG\Property(property="content", type="string", example="Isi Berita")
     * @SWG\Property(property="status", type="integer", example=1, default=0, description="0 - draft <br>1 - published")
     * @SWG\Property(property="created_at", type="string", example="2019-04-22 01:38:59")
     * @SWG\Property(property="updated_at", type="string", example="2019-04-22 01:38:59")
     *
     */


}
