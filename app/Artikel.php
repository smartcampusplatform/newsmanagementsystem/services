<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $id_fakultas
 * @property string $guid
 * @property int $modified_by
 * @property string $link
 * @property string $title
 * @property string $content
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 * @property Fakultas $fakulta
 */

/**
 * expecting "definition", "ref", "title", "description", "maxProperties",
 * "minProperties", "required", "properties", "type", "format", "items",
 * "collectionFormat", "default", "maximum", "exclusiveMaximum",
 * "minimum", "exclusiveMinimum", "maxLength", "minLength",
 * "pattern", "maxItems", "minItems", "uniqueItems", "enum",
 * "multipleOf", "discriminator", "readOnly", "xml", "externalDocs",
 * "example", "allOf", "additionalProperties", "x"
 */
/**
 *
 * @SWG\Definition(
 * )
 * Class Artikel
 * @package App
 */
class Artikel extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'artikel';

    /**
     * @var array
     */
    protected $fillable = ['id_fakultas', 'guid', 'modified_by', 'link', 'title', 'content', 'status'];

    protected $hidden=['id_fakultas'];

    protected $appends=['fakultas_name'];

    public function getFakultasNameAttribute($value)
    {
        $fakultas_name=null;
        if($this->fakulta)
        {
            $fakultas_name=$this->fakulta->nama_fakultas;
        }
        return $fakultas_name;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function fakulta()
    {
        return $this->belongsTo('App\Fakultas', 'id_fakultas');
    }

    /**
     * @SWG\Property(property="id", type="integer", example=1,)
     * @SWG\Property(property="id_fakultas", type="integer", example=1, description="ID Fakultas ITB")
     * @SWG\Property(property="guid", type="string", example="https://fitb.itb.ac.id/?p=3669")
     * @SWG\Property(property="modified_by", type="integer", example=1, description="userid")
     * @SWG\Property(property="link", type="string", example="http://fitb.itb.ac.id/2018/08/08/kunjungan-mahasiswa-teknik-geologi-universitas-patimura/", description="link content")
     * @SWG\Property(property="title", type="string", example="Kunjungan Mahasiswa Teknik Geologi Universitas Patimura", description="title of article")
     * @SWG\Property(property="content", type="string", example="Content Article", description="content of article")
     * @SWG\Property(property="status", type="integer", example=1, default=1, description="0- draft<br>1- published")
     * @SWG\Property(property="created_at", type="string", example="2019-04-22 01:38:59")
     * @SWG\Property(property="updated_at", type="string", example="2019-04-22 01:38:59")
     */
}
