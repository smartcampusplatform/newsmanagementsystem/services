<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $soal_kuesioner
 * @property string $jns_kuesioner
 * @property string $created_at
 * @property string $updated_at
 * @property User $user
 */
class Kuesioner extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'tm_kuesioner';

    /**
     * @var array
     */
    protected $fillable = ['id', 'soal_kuesioner', 'jns_kuesioner','created_at', 'updated_at'];

    public $timestamps = false;
   
}
