<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $id_user
 * @property integer $tipe
 * @property string $hasil
 * @property string $created_at
 * @property string $updated_at
 * @property User $user
 */
class Feedback extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['id_user', 'tipe', 'hasil'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'id_user');
    }
}
