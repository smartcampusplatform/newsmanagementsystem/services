<?php
/**
 * Created by PhpStorm.
 * User: Asus
 * Date: 03/04/2019
 * Time: 20:02
 */

namespace App\Http\Controllers;

use App\Fakultas;
use Illuminate\Http\Request;
use Swagger\Annotations as SWG;


/**
 * Class FakultasController
 * @package App\Http\Controllers
 */

class FakultasController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth',['except'=>['index']]);
    }

    /**
     * @SWG\Get(
     *   path="/faculties",
     *   operationId="index",
     *   summary="Get Faculties Detail registered in ITB",
     *   tags={"faculties"},
     *     @SWG\Parameter(
     *         name="u",
     *         in="query",
     *         description="Token SSO from IdentityManagement",
     *         required=true,
     *         type="string"
     *     ),
     *   @SWG\Response(
     *     response=200,
     *     description="Working"
     *   ),
     *   @SWG\Response(
     *     response="default",
     *     description="an ""unexpected"" error",
     *     @SWG\Schema(ref="#/definitions/Fakultas"),
     *
     *   )
     * )
     */
    public function index()
    {
        $fakultas = Fakultas::all()->
                    where('status','=',true)->sortBy('id');
        return response()->json(json_decode($fakultas),200,[],JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);
    }


    /**
     * @SWG\Get(
     *     path="/faculties/{id}/",
     *     operationId="/faculties/id",
     *     tags={"faculties"},
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="ID Fakultas",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="u",
     *         in="query",
     *         description="Token SSO",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *     response="200",
     *     description="Returns some faculty web address",
     *     @SWG\Schema(ref="#/definitions/Fakultas"),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="Error: Bad request. When required parameters were not supplied.",
     *     ),
     * )
     */
    public function show($id)
    {
        $fakultas = Fakultas::find($id);
        return response()->json(json_decode($fakultas),200,[],JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);
    }

    /**
     * @SWG\Post(
     *     path="/faculties",
     *     operationId="store",
     *     tags={"faculties"},
     *     summary="Store the detail of Faculties",
     *     @SWG\Parameter(
     *     in="body",
     *     name="body",
     *     description="The faculty to create",
     *     @SWG\Schema(
     *     @SWG\Property(
     *     property="nama_fakultas",
     *     type="string"
     *      ),
     *     @SWG\Property(
     *     property="url_web",
     *     type="string"
     *      ),
     *     @SWG\Property(
     *     property="nama_tag",
     *     type="string"
     *      ),
     *     )
     *   ),
     *     @SWG\Parameter(
     *     in="query",
     *      name="u",
     *     type="string",
     * description="user token from SSO identity manager"
     * ),
     *     @SWG\Response(
     *     response=201,
     *     description="Sukses"
     *   ),
     *   @SWG\Response(
     *     response="default",
     *     description="an unexpected error"
     *   )
     * ),
     *
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        Fakultas::create($request->all());

        return response()->json(['message'=>'Sukses',
        ],201);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $fakultas = Fakultas::find($id);

        $nama_fakultas=$request->get('nama_fakultas');
        $url_web=$request->get('url_web');
        $nama_tag=$request->get('nama_tag');
        $status=$request->get('status');


        $fakultas->nama_fakultas =$nama_fakultas? $nama_fakultas :$fakultas->nama_fakultas;
        $fakultas->url_web =$url_web? $url_web :$fakultas->url_web;
        $fakultas->nama_tag=$nama_tag? $nama_tag :$fakultas->nama_tag;
        $fakultas->status=$status? $status:$fakultas->status;


//        $data=$request->all();
//        $fakultas->fill($data);



        if($fakultas->save())
        {
            return response()->json([
                'status'=>'Success',
                'message'=>'Resource Updated',
                'data'=>$this->show($fakultas->id)],
                200,[],JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);
        }
        else return response()->json(['status' => 'fail'],401);
    }

    public function delete($id)
    {
        $fakultas = Fakultas::find($id);
        $fakultas->update(['status'=>0]);
        return response()->json(['message' => 'Successfully delete faculty']);
    }


}