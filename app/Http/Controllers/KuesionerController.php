<?php
/**
 * Created by PhpStorm.
 * User: Asus
 * Date: 15/04/2019
 * Time: 15:10
 */
namespace App\Http\Controllers;
use App\Kuesioner;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class KuesionerController extends Controller
{
    public function index()
    {
        $kuesioner= Kuesioner::all();
        return response()->json(json_decode($kuesioner),200,[],JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);
    }

    public function create(Request $request)
    {
        Kuesioner::create($request->all());
        return response()->json(['code'=>'Sukses',
        ],201);
    }

    public function show($id)
    {
        $kuesioner = Fakultas::find($id);
        return response()->json(json_decode($kuesioner),200,[],JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);
    }

    public function update(Request $request, $id)
    {
        $kuesioner = Fakultas::find($id);

        $soal_kuesioner=$request->get('soal_kuesioner');
        $jns_kuesioner=$request->get('jns_kuesioner');


        $kuesioner->soal_kuesioner =$soal_kuesioner? $soal_kuesioner :$kuesioner->soal_kuesioner;
        $kuesioner->jns_kuesioner =$jns_kuesioner? $jns_kuesioner :$kuesioner->jns_kuesioner;


        if($kuesioner->save())
        {
            return response()->json([
                'status'=>'Success',
                'message'=>'Resource Updated',
                'data'=>$this->show($kuesioner->id)],
                200,[],JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);
        }
        else return response()->json(['status' => 'fail'],401);
    }

    public function delete($id)
    {
        $kuesioner = Fakultas::find($id);
        $kuesioner->update(['status'=>0]);
        return response()->json(['message' => 'Successfully delete faculty']);
    }

}