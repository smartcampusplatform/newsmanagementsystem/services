<?php
/**
 * Created by PhpStorm.
 * User: Asus
 * Date: 03/04/2019
 * Time: 18:20
 */

namespace App\Http\Controllers;


use App\Article;
use App\Artikel;
use App\Fakultas;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Swagger\Annotations as SWG;

class GrabNewsController extends Controller
{
    public function __construct()
    {
//        $this->middleware('auth',['except'=>['index']]);
    }


    /**
     * @SWG\Get(
     *     path="/scrap",
     *     operationId="scrap",
     *     summary="get news title and link from all faculties in ITB",
     *     tags={"scrap"},
     *     @SWG\Parameter(
     *         name="u",
     *         in="query",
     *     required=true,
     *         description="Token SSO from Identitymanagement",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *     response="200",
     *     description="Returns some article from various web faculties in ITB",
     *     ),
     *     @SWG\Response(
     *         response="500",
     *         description="Internal Server Error. When required parameters were not supplied.",
     *     ),
     * )
     * menggunakan GuzzleHTTP
     */
    public function scrap()
    {
        $total=0;
        $hasil=[];
        $fakultas=Fakultas::all();
//        $artikel = new Artikel();
        $client = new Client([
            'verify' => false
        ]);

        foreach ($fakultas as $fak)
        {
            $id_fakultas=$fak->id;
//            punya STEI masih error blm tahu penyebabnya, kemungkinan konfigurasi wordpressnya
            if($id_fakultas==12||$id_fakultas==9||$id_fakultas==5)
                continue;
            $url_web= $fak->url_web;
            $nama_tag=$fak->nama_tag;


            $url=$url_web."/wp-json/wp/v2/posts/?filter[category_name]=".$nama_tag."&per_page=5&page=1&_fields[]=id&_fields[]=date&_fields[]=title&_fields[]=guid&_fields[]=modified&_fields[]=status&_fields[]=link&_fields[]=title";
            $response = $client->get($url);
            $status = $response->getStatusCode();
            if($status==200)
            {
                $decode=  $response->getBody();
                $content=json_decode($decode->getContents());
                foreach ($content as $con)
                {
                    array_push($hasil,$con);
                }


            }
            else
                continue;
//            misal 404
        }
        return response()->json( $hasil,200,[],JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);

    }


    /**
     *      * @SWG\Get(
     *     path="/scrap/{id}",
     *     operationId="news",
     *     summary="get news title and link from certain faculties based ID in ITB",
     *     tags={"scrap"},
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *     required=true,
     *         description="ID of web faculties in ITB",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="u",
     *         in="query",
     *     required=true,
     *         description="Token SSO from Identitymanagement",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *     response="200",
     *     description="Returns some article from various web faculties in ITB",
     *     ),
     *     @SWG\Response(
     *         response="500",
     *         description="Internal Server Error. When required parameters were not supplied.",
     *     ),
     * )
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function news($id)
    {
        $fakultas=Fakultas::find($id);
        $url_web= $fakultas->url_web;
        $nama_tag=$fakultas->nama_tag;


        $url=$url_web."/wp-json/wp/v2/posts/?filter[category_name]=".$nama_tag.
            "&per_page=10&_fields[]=id&_fields[]=date&_fields[]=title&_fields[]=guid&_fields[]=modified&_fields[]=status&_fields[]=link&_fields[]=title";
        $client = new Client([
            'verify' => false
        ]);
        $response = $client->get($url);
        $status = $response->getStatusCode();
        if($status==200)
        {
            $decode=  $response->getBody();
            $content=json_decode($decode->getContents());

//
//            }
            return response()->json($content,200,[],JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);
        }
        else
            return response()->json(['message'=>'Not Found'],404,[],JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);


    }


    /**
     *     @SWG\Get(
     *     path="/scrap/{id}/detail/{id_post}",
     *     operationId="postIs",
     *     summary="get full content news from web faculties based post id  in ITB",
     *     tags={"scrap"},
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *     required=true,
     *         description="ID of web faculties in ITB",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="id_post",
     *         in="path",
     *     required=true,
     *         description="post ID from web",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="u",
     *         in="query",
     *     required=true,
     *         description="Token SSO from Identitymanagement",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *     response="200",
     *     description="Returns some article from various web faculties in ITB",
     *     ),
     *     @SWG\Response(
     *         response="500",
     *         description="Internal Server Error. When required parameters were not supplied.",
     *     ),
     * )
     *
     * @param $id
     * @param $id_post
     * @return \Illuminate\Http\JsonResponse
     */
    public function postId($id, $id_post)
    {
        $fakultas=Fakultas::whereId($id)->get()->first();
        $url_web= $fakultas->url_web;
//        dd($url_web);
        $url = '/wp-json/wp/v2/posts/?include[]='.$id_post.'&_fields[]=id&_fields[]=date&_fields[]=title&_fields[]=guid&_fields[]=modified&_fields[]=status&_fields[]=link&_fields[]=title&_fields[]=content';
        $client = new Client(['base_uri'=>$url_web]);
        $response = $client->get($url,['verify'=>false]);
        $status = $response->getStatusCode();
        if($status==200)
        {
            $decode=  $response->getBody();
            $content=json_decode($decode->getContents());
            return response()->json($content,200,[],JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);
        }
        else{
            return response()->json(['message'=>'Not Found'],404,[],JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);
        }
    }

    /**
     *
     *     @SWG\Get(
     *     path="/scrap/{id}/flash/{num}",
     *     operationId="flash",
     *     summary="get flash news in certain faculties",
     *     tags={"scrap"},
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *     required=true,
     *         description="ID of web faculties in ITB",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="u",
     *         in="query",
     *     required=true,
     *         description="Token SSO from Identitymanagement",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *     response="200",
     *     description="Returns some article from various web faculties in ITB",
     *     ),
     *     @SWG\Response(
     *         response="500",
     *         description="Internal Server Error. When required parameters were not supplied.",
     *     ),
     * )
     * @param $num
     * @return \Illuminate\Http\JsonResponse
     */
    public function flashNews($id)
    {
        $fakultas=Fakultas::find($id);
        $url_web= $fakultas->url_web;
        $nama_tag=$fakultas->nama_tag;
            $num= 1;
        $url=$url_web."/wp-json/wp/v2/posts/?filter[category_name]=".$nama_tag.
            "&per_page=".$num."&_fields[]=id&_fields[]=date&_fields[]=title&_fields[]=guid&_fields[]=modified&_fields[]=status&_fields[]=link&_fields[]=title&_fields[]=content";
        $client = new Client([
            'verify' => false
        ]);
        $response = $client->get($url);
        $status = $response->getStatusCode();
        if($status==200)
        {
            $decode=  $response->getBody();
            $content=json_decode($decode->getContents());
            return response()->json($content,200,[],JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);
        }
        else
            return response()->json(['message'=>'Not Found'],404,[],JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);


    }

    public function getDistance($latitude1, $longitude1, $latitude2, $longitude2) {
        $earth_radius = 6371;

        $dLat = deg2rad($latitude2 - $latitude1);
        $dLon = deg2rad($longitude2 - $longitude1);

        $a = sin($dLat/2) * sin($dLat/2) + cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * sin($dLon/2) * sin($dLon/2);
        $c = 2 * asin(sqrt($a));
        $d = $earth_radius * $c;

        $jarak = $d*1000; // in meter
//        return response()->json(['data'=>$jarak],200,[],JSON_PRETTY_PRINT);
        return $jarak;
    }



    /**
     * @SWG\Get(
     *     path="/nearby",
     *     operationId="nearbyFakultas",
     *     summary="get flash news based user location ITB",
     *     tags={"scrap"},
     *     @SWG\Parameter(
     *         name="lat",
     *         in="query",
     *     required=true,
     *         description="Latitude of position",
     *         required=true,
     *         type="string",
     *         @SWG\Schema(
     *     example="-6.890631",
     * )
     *     ),
     *     @SWG\Parameter(
     *         name="long",
     *         in="query",
     *     required=true,
     *         description="Longitude of position",
     *         type="string",
     *     @SWG\Schema(
     *     example="107.610556",
     * )
     *
     *     ),
     *     @SWG\Response(
     *     response="200",
     *     description="Returns some article from various web faculties in ITB",
     *     ),
     *     @SWG\Response(
     *         response="500",
     *         description="Internal Server Error. When required parameters were not supplied.",
     *     ),
     * )
     * menggunakan GuzzleHTTP
     */
    public function nearbyFakultas(Request $request)
    {
        $hasil=[];
        $lokasi=[];
        $lat= $request->input('lat');
        $long= $request->input('long');

        $all = Fakultas::all();
//        dd($all);
        foreach ($all as $fakultas)
        {

            $latitude2= $fakultas->lat;
            $longitude2= $fakultas->long;
            $jarak  = $this->getDistance($lat, $long, $latitude2, $longitude2);
            {
                $id = $fakultas->id;
                array_push($hasil,$id);
                array_push($lokasi,$jarak);
            };
        }
        $idx=array_search(min($lokasi),$lokasi);
        $news=$this->flashNews($hasil[$idx]);



//        return response()->json(json_decode($news),200,[],JSON_PRETTY_PRINT);
        return $news;
    }

    public function jarakFakultas($id1, $id2) {
        $fakultas1= Fakultas::find($id1);
        $fakultas2= Fakultas::find($id2);

        $latitude1= $fakultas1->lat;
        $latitude2= $fakultas2->lat;
        $longitude1= $fakultas1->long;
        $longitude2= $fakultas2->long;

        $earth_radius = 6371;

        $dLat = deg2rad($latitude2 - $latitude1);
        $dLon = deg2rad($longitude2 - $longitude1);

        $a = sin($dLat/2) * sin($dLat/2) + cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * sin($dLon/2) * sin($dLon/2);
        $c = 2 * asin(sqrt($a));
        $d = $earth_radius * $c;

//        $jarak = $d*1000; // in meter
//        return response()->json(['data'=>$jarak],200,[],JSON_PRETTY_PRINT);
        return $d;
    }

}