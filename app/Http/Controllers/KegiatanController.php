<?php
/**
 * Created by PhpStorm.
 * User: Asus
 * Date: 22/04/2019
 * Time: 6:52
 */

namespace App\Http\Controllers;


use App\Kegiatan;
use Illuminate\Http\Request;

class KegiatanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth',['except'=>['index']]);
    }

    /**
     *   @SWG\Get(
     *   path="/kegiatan",
     *   operationId="index",
     *   summary="Get All Activities Reporting Agenda Detail registered in ITB",
     *   tags={"kegiatan"},
     *   @SWG\Response(
     *     response=200,
     *     description="Working"
     *   ),
     *   @SWG\Response(
     *     response="default",
     *     description="an ""unexpected"" error",
     *   )
     * )
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $kegiatan= Kegiatan::all()->toArray();
        return response()->json(array_values($kegiatan),200,[],JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);
    }


    /**
     * @SWG\Get(
     *     path="/kegiatan/{id}/",
     *     operationId="show",
     *     summary="Get one activities detail from specified ID",
     *     tags={"kegiatan"},
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="ID kegiatan",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="u",
     *         in="query",
     *         description="Token SSO",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *     response="200",
     *     description="Returns some faculty web address",
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="Error: Bad request. When required parameters were not supplied.",
     *     ),
     * )
     */
    public function show($id)
    {
        $kegiatan = Kegiatan::find($id);
        return response()->json(json_decode($kegiatan),200,[],JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);
    }

    /**
     * @SWG\Post(
     *     path="/kegiatan",
     *     operationId="store",
     *     tags={"kegiatan"},
     *     summary="Store the item of activities",
     *     @SWG\Parameter(
     *     in="body",
     *     name="body",
     *     description="The Agenda to create",
     *     @SWG\Schema(
     *     @SWG\Property(
     *      property="nama_agenda",
     *     example="Event A",
     *     type="string"
     *              ),
     *     @SWG\Property(
     *      property="tanggal",
     *     example="2019-04-30",
     *     type="string"
     *              ),
     *     @SWG\Property(
     *      property="organizer",
     *     example="Fakultas A",
     *     type="string"
     *              ),
     *     @SWG\Property(
     *      property="category_event",
     *     example="International",
     *
     *     type="string"
     *              ),
     *     @SWG\Property(
     *      property="partisipan",
     *     example="Umum, Mahasiswa",
     *     type="string"
     *              ),
     *     @SWG\Property(
     *      property="link_agenda",
     *     example="http://test.com",
     *     type="string"
     *              ),
     *     @SWG\Property(
     *      property="deskripsi",
     *     example="Contoh Deskripsi",
     *     type="string"
     *              ),
     *     @SWG\Property(
     *      property="status",
     *     type="integer",
     *     default=0,
     *              ),
     *          )
     *      ),
     *     @SWG\Parameter(
     *     in="query",
     *      name="u",
     *     type="string",
     * description="user token from SSO identity manager"
     * ),
     *     @SWG\Response(
     *     response=201,
     *     description="Success"
     *   ),
     *   @SWG\Response(
     *     response="default",
     *     description="an unexpected error"
     *   )
     *
     * )
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $kegiatan=Kegiatan::create($request->all());

        return response()->json(['message'=>'Sukses',
            'data'=>$kegiatan
        ],201);

    }



}