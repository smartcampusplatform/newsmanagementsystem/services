<?php
/**
 * Created by PhpStorm.
 * User: Asus
 * Date: 21/04/2019
 * Time: 22:39
 */

namespace App\Http\Controllers;


use App\Komentar;
use App\User;
use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Swagger\Annotations as SWG;

class KomentarController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @SWG\Get(
     *   path="/comments",
     *   operationId="index",
     *   summary="Get All Comments across all News from ITB web",
     *   tags={"comments"},
     *     @SWG\Parameter(
     *     in="query",
     *      name="u",
     *     type="string",
     * description="user token from SSO identity manager"
     * ),
     *   @SWG\Response(
     *     response=200,
     *     description="Working"
     *   ),
     *   @SWG\Response(
     *     response="default",
     *     description="an ""unexpected"" error",
     *   )
     * )
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $user = $request->user()->login_id;
        if ($request->user()) {
            $komentar = Komentar::all();
            return response()->json(json_decode($komentar), 200, [], JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);

        }
    }
    /**
     * @SWG\Get(
     *     path="/news/{id}/comments",
     *     operationId="show",
     *     summary="Get list of commpents of specified ID news",
     *     tags={"news","comments"},
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="ID news",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="u",
     *         in="query",
     *         description="Token SSO",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *     response="200",
     *     description="Returns some faculty web address",
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="Error: Bad request. When required parameters were not supplied.",
     *     ),
     * )
     */
    public function show(Request $request, $id)
    {
        $list_komentar = Komentar::all()->where('id_berita', '=', $id)->sortBy('created_at', 0, true)->toArray();
        return response()->json(['message' => 'Sukses',
            'data' => array_values($list_komentar)], 200, [], JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
    }

    /**
     * @SWG\Post(
     *     path="/comments",
     *     operationId="store",
     *     tags={"comments"},
     *     summary="Create News Comments include tags",
     *      @SWG\Parameter(
     *      in="body",
     *      name="body",
     *      description="The comments to create",
     *      @SWG\Schema(
     *          @SWG\Property(
     *          property="id_berita",
     *          example=1,
     *          type="integer"
     *          ),
     *     @SWG\Property(
     *      property="comment",
     *     example="This is the example of the comment",
     *     type="string"
     *             ),
     *      ),),
     *     @SWG\Parameter(
     *     in="query",
     *      name="u",
     *     type="string",
     * description="user token from SSO identity manager"
     *  ),
     *     @SWG\Response(
     *     response=201,
     *     description="Success"
     *       ),
     *   @SWG\Response(
     *     response="default",
     *     description="an unexpected error",
     *   )
     * )
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {

        $id_user = null;

//        Ubah ke cek token SSO Identity
//        if ($request->header('Authorization')) {
//            $apiToken=explode(' ',$request->header('Authorization'));
//            $user= User::where('api_token', $apiToken[1])->first();
//            $id_user= $user->id;
//        }
        $user = $request->user()->login_id;
        if ($request->user()) {
            $komentar = new Komentar();
            $id_user = User::whereUsername($user)->first()->id;
            $komentar->id_user = $id_user;
            $id_berita = $request->input('id_berita');
            $komentar->id_berita = $id_berita;
            $komentar->id_user = $id_user;
            $komentar->comment = $request->input('comment');
            if ($komentar->save()) {
                return response()->json(['message' => 'Success',
                    'data' => $komentar->only(['id', 'user_name', 'berita_title', 'comment'])
                ], 201);
            } else
                return response()->json(['message' => 'Failed',
                ], 400);
        }


    }


    public function hate()
    {
//        Get record uninspected comment
        $list_komentar = [];
        $komentar = Komentar::all()->whereStrict('hate_speech', null);
        $arr = array("text" => "");
//        TODO securing x-api-key method
        $client = new Client([
            'verify' => false,
            'headers' => [
                'x-api-key' => 'Ds2eTSmwbQWT1oVZscdxtGYqA9Z7lu4Mm4xJ8LA0',
                'Content-Type' => 'application/json'
            ]
        ]);
        $url = "https://api.prosa.ai/v1/hates";
        foreach ($komentar as $kom) {
            $arr['text'] = $kom->comment;
            $text = json_encode($arr);
            $response = $client->post($url, ['body' => $text]);
            $status = $response->getStatusCode();
            if ($status == 200) {
                $content = json_decode($response->getBody()->getContents());
//                return response()->json(json_decode($response->getBody()->getContents()));
                $kom->hate_speech = $content->hate_speech;
                $kom->confidence = $content->confidence;
                if ($kom->save()) {
                    array_push($list_komentar, $kom);
//                    return response()->json($kom);
                }
            } else
                continue;
        }
        return response()->json($list_komentar);

    }

    /**
     * @SWG\Get(
     *     path="/comments/hatespeech",
     *     operationId="getHate",
     *     tags={"comments"},
     *     summary="get a list of comments suspected of being hatespeech",
     *     @SWG\Parameter(
     *     in="query",
     *      name="u",
     *     type="string",
     * description="user token from SSO identity manager"
     *      ),
     *     @SWG\Response(
     *     response="200",
     *     description="Returns some faculty web address",
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="Error: Bad request. When required parameters were not supplied.",
     *     ),
     * )
     * @return \Illuminate\Http\JsonResponse
     */
    public function getHate(Request $request)
    {
        $user = $request->user()->login_id;
        if ($request->user()&&($request->user()->login_role==0 ||$request->user()->login_role==3)) {

            $list_hate = Komentar::hate()->orWhere->confidence()->get()->toArray();
            return response()->json(array_values($list_hate), 200, [], JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
        }
        else {
            return response()->json(['message' => 'Admin page only'], 401, [], JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
        }
    }

}