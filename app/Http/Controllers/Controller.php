<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Swagger\Annotations as SWG;

class Controller extends BaseController
{
    /**
     * @SWG\Swagger(
     *     host="178.128.104.74",
     *     basePath="/newsmanagementsystem",
     *
     * )
     * @SWG\Info(
     *   title="API Documentation for News MS",
     *   version="0.1a",
     *   @SWG\Contact(
     *     email="roufiq@s.itb.ac.id",
     *     name="Smart Campus 2nd Group"
     *   )
     * )
     */
    //
}
