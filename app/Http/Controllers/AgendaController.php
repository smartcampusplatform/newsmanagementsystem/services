<?php
/**
 * Created by PhpStorm.
 * User: Asus
 * Date: 15/04/2019
 * Time: 15:10
 */
namespace App\Http\Controllers;
use App\Agenda;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Swagger\Annotations as SWG;

class AgendaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth',['except'=>['index','verify']]);
    }


    /**
     *   @SWG\Get(
     *   path="/agenda",
     *   operationId="index",
     *   summary="Get All Published and Verified Agenda Detail registered in ITB",
     *   tags={"agenda"},
     *   @SWG\Response(
     *     response=200,
     *     description="Working"
     *   ),
     *   @SWG\Response(
     *     response="default",
     *     description="an ""unexpected"" error",
     *   )
     * )
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $agenda= Agenda::publish()->orderBy('created_at','desc')->get()->toArray();
        return response()->json(array_values($agenda),200,[],JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);
    }

    /**
     *   @SWG\Get(
     *   path="/agenda/draft",
     *   operationId="draft",
     *   summary="Get All Draft Agenda Detail registered in ITB",
     *     description="in order to gain access, token must be as admin (0) or PR(3)",
     *   tags={"agenda"},
     *     @SWG\Parameter(
     *         name="u",
     *         in="query",
     *         description="Token SSO from IdentityManagement",
     *         required=true,
     *         type="string"
     *     ),
     *   @SWG\Response(
     *     response=200,
     *     description="Working"
     *   ),
     *     @SWG\Response(
     *     response=401,
     *     description="Error: Unauthorized"
     *   ),
     *   @SWG\Response(
     *     response="default",
     *     description="an ""unexpected"" error",
     *   )
     * )
     * @return \Illuminate\Http\JsonResponse
     */
    public function draft(Request $request)
    {
        $user= $request->user();
//      $user =Auth::user();
//        dd($role);
        if(!empty($user)&&($user->login_role==0 ||$user->login_role==3))
        {
            $agenda= Agenda::draft()->orderBy('created_at','desc')->get()->toArray();
            return response()->json(array_values($agenda),200,[],JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);
        }
        else{
            return response()->json(['message'=>'Only Staff and Admin Page!'],401,[],JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);
        }

    }

    /**
     * @SWG\Post(
     *     path="/agenda",
     *     operationId="create",
     *     tags={"agenda"},
     *     summary="Store the item of Agenda",
     *     @SWG\Parameter(
     *     in="body",
     *     name="body",
     *     description="The Agenda to create",
     *     @SWG\Schema(
     *     @SWG\Property(
     *      property="nama_agenda",
     *     example="Event A",
     *     type="string"
     *              ),
     *     @SWG\Property(
     *      property="tanggal",
     *     example="2019-04-30",
     *     type="string"
     *              ),
     *     @SWG\Property(
     *      property="organizer",
     *     example="Fakultas A",
     *     type="string"
     *              ),
     *     @SWG\Property(
     *      property="category_event",
     *     example="International",
     *
     *     type="string"
     *              ),
     *     @SWG\Property(
     *      property="partisipan",
     *     example="Umum, Mahasiswa",
     *     type="string"
     *              ),
     *     @SWG\Property(
     *      property="link_agenda",
     *     example="http://test.com",
     *     type="string"
     *              ),
     *     @SWG\Property(
     *      property="deskripsi",
     *     example="Contoh Deskripsi",
     *     type="string"
     *              ),
     *     @SWG\Property(
     *      property="status",
     *     type="integer",
     *     default=0,
     *              ),
     *          )
     *      ),
     *     @SWG\Parameter(
     *     in="query",
     *      name="u",
     *     type="string",
     * description="user token from SSO identity manager"
     * ),
     *     @SWG\Response(
     *     response=201,
     *     description="Success"
     *   ),
     *   @SWG\Response(
     *     response="default",
     *     description="an unexpected error"
     *   )
     *
     * )
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request)
    {
        $id_author=null;
        $user= $request->user()->login_id;
        if($request->user())
        {
            $id_author= User::whereUsername($user)->first()->id;
            $agenda= new Agenda();
            $agenda->id_author=$id_author;
            $agenda->nama_agenda= $request->input('nama_agenda');
            $agenda->tanggal=$request->input('tanggal');
            $agenda->organizer= $request->input('organizer');
            $agenda->category_event= $request->input('category_event');
            $agenda->partisipan= $request->input('partisipan');
            $agenda->link_agenda= $request->input('link_agenda');
            $agenda->deskripsi= $request->input('deskripsi');
            $agenda->status=0;

            if($agenda->save())
            {
//            Agenda::create($request->all());
                return response()->json(['message'=>'Success!',
                    'data'=>$agenda
                ],201,[],JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);
            }
            else
                return response()->json(['message'=>'Fail',
                    'data'=>$agenda
                ],400,[],JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);
        }
        else
            return response()->json(['message'=>'Only registered user',
            ],400,[],JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);


    }

    /**
     * @SWG\Post(
     *     path="/agenda/verify/{id}",
     *     operationId="verify",
     *     tags={"agenda"},
     *     summary="Verify the item of Agenda",
     *     description="in order to gain access the token must be admin role (0) or PR(3)",
     *     @SWG\Parameter(
     *     in="body",
     *     name="body",
     *     description="The Agenda to create",
     *     @SWG\Schema(
     *     @SWG\Property(
     *      property="nama_agenda",
     *     example="Event A",
     *     type="string"
     *              ),
     *     @SWG\Property(
     *      property="tanggal",
     *     example="2019-04-30",
     *     type="string"
     *              ),
     *     @SWG\Property(
     *      property="organizer",
     *     example="Fakultas A",
     *     type="string"
     *              ),
     *     @SWG\Property(
     *      property="category_event",
     *     example="International",
     *
     *     type="string"
     *              ),
     *     @SWG\Property(
     *      property="partisipan",
     *     example="Umum, Mahasiswa",
     *     type="string"
     *              ),
     *     @SWG\Property(
     *      property="link_agenda",
     *     example="http://test.com",
     *     type="string"
     *              ),
     *     @SWG\Property(
     *      property="deskripsi",
     *     example="Contoh Deskripsi",
     *     type="string"
     *              ),
     *          )
     *      ),
     *     @SWG\Parameter(
     *     in="path",
     *      name="id",
     *     type="string",
     * description="id agenda status draft"
     * ),
     *
     *     @SWG\Parameter(
     *     in="query",
     *      name="u",
     *     type="string",
     * description="user token from SSO identity manager"
     * ),
     *     @SWG\Response(
     *     response=201,
     *     description="Success"
     *   ),
     *   @SWG\Response(
     *     response="default",
     *     description="an unexpected error"
     *   )
     *
     * )
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function verify(Request $request, $id)
    {
        $user= $request->user();
//      $user =Auth::user();
//        dd($role);
        if(!empty($user)&&($user->login_role==0 ||$user->login_role==3))
        {

            $agenda = Agenda::find($id);
            $id_verificator= User::whereUsername($user->login_id)->first()->id;
            $nama_agenda= $request->input('nama_agenda');
            $tanggal=$request->input('tanggal');
            $organizer= $request->input('organizer');
            $category_event= $request->input('category_event');
            $partisipan= $request->input('partisipan');
            $link_agenda= $request->input('link_agenda');
            $deskripsi= $request->input('deskripsi');
            $agenda->nama_agenda=$nama_agenda? $nama_agenda : $agenda->nama_agenda;
            $agenda->tanggal=$tanggal? $tanggal: $agenda->tanggal;
            $agenda->organizer= $organizer? $organizer: $agenda->organizer;
            $agenda->category_event= $category_event?$category_event: $agenda->category_event;
            $agenda->partisipan= $partisipan?$partisipan:$agenda->partisipan;
            $agenda->link_agenda= $link_agenda?$link_agenda:$agenda->link_agenda;
            $agenda->deskripsi=$deskripsi?$deskripsi:$agenda->deskripsi;

            $agenda->status=1;
            $agenda->id_verificator=$id_verificator;
            if ($agenda->save())
            {
                return response()->json([
                    'status'=>'Success',
                    'message'=>'Resource Updated',
                    'data'=>$agenda],
                    200,[],JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);
            }
            else return response()->json(['status' => 'fail'],401);

        }
        else
            return response()->json(['message'=>'Only admin',
            ],400,[],JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);

    }

    /**
     * @SWG\Get(
     *     path="/agenda/detail/{id}/",
     *     operationId="/agenda",
     *     summary="Get one agenda detail from specified ID",
     *     tags={"agenda"},
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="ID agenda",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="u",
     *         in="query",
     *         description="Token SSO",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *     response="200",
     *     description="Returns some faculty web address",
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="Error: Bad request. When required parameters were not supplied.",
     *     ),
     * )
     */
    public function show(Request $request,$id)
    {
        if($request->user())
        {
            $user = $request->user()->login_id;
            $agenda = Agenda::find($id);
            return response()->json(json_decode($agenda),200, [], JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
        }
        else {
            return response()->json(['message' => 'Registered user page only'], 401, [], JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
        }

    }

}