<?php
/**
 * Created by PhpStorm.
 * User: Asus
 * Date: 01/05/2019
 * Time: 16:20
 */

namespace App\Http\Controllers;


use GuzzleHttp\Client;

class CalendarController extends Controller
{
    public function holiday($year)
    {

        $url= "https://www.googleapis.com/calendar/v3/calendars/id.indonesian%23holiday%40group.v.calendar.google.com/events?maxResults=250&orderBy=startTime&singleEvents=true&timeMax=".$year."-12-31T23%3A59%3A59Z&timeMin=".$year."-01-01T00%3A00%3A00Z&fields=items(description%2Cend%2Fdate%2Cid%2Cstart%2Fdate%2Cstatus%2Csummary%2Cupdated)&key=AIzaSyB1Cw55g2_vcmyje5jaYO6rorAFJdjV4dU";
        $client= new Client([
            'verify'=>false
        ]);
        $response = $client->get($url);
        $status = $response->getStatusCode();
        if ($status=200)
        {
            $content=json_decode($response->getBody()->getContents());
            return response()->json($content,200,[],JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);
        }
        else
            return response()->json(['message'=>'Not Found'],404,[],JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);


    }
}