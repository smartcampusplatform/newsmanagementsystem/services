<?php

namespace App\Http\Controllers;

use App\Artikel;
use Illuminate\Http\Request;

class ArtikelController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth',['except'=>['index']]);
    }

    /**
     * mendapatkan seluruh artikel dari hasil grab
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $artikel =Artikel::all()->toArray();
        return response()->json(array_values($artikel),200,[],JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);
    }

    public function store(Request $request)
    {
        $artikel = new Artikel();
//        $artikel->id_fakultas=

    }




}
