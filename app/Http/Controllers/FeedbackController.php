<?php
/**
 * Created by PhpStorm.
 * User: Asus
 * Date: 15/04/2019
 * Time: 15:10
 */
namespace App\Http\Controllers;
use App\Feedback;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FeedbackController extends Controller
{
    public function index()
    {
        $feedback= Feedback::all();
        return response()->json(json_decode($feedback),200,[],JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);
    }

    public function create(Request $request)
    {
        Feedback::create($request->all());
        return response()->json(['code'=>'Sukses',
        ],201);
    }

}