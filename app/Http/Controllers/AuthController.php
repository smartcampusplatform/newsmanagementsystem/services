<?php
/**
 * Created by PhpStorm.
 * User: Asus
 * Date: 21/04/2019
 * Time: 16:50
 */

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;
use Illuminate\Support\Str;
use Swagger\Annotations as SWG;

class AuthController extends Controller
{



    /**
     * @SWG\Post(
     *  path="/register",
     *     deprecated=true,
     *  description="deprecated, see identitymanager to get access",
     *  operationId="register",
     * summary="Register User",
     *  tags={"user"},
     *     @SWG\Parameter(
     *     name="User",
     *     in="body",
     *     description="User to create",
     *     @SWG\Schema(
     *      @SWG\Property(
     *      property="username",
     *      example="John Doe",
     *      type="string",
     *      ),
     *      @SWG\Property(
     *      property="email",
     *      example="example@students.itb.ac.id",
     *       type="string",
     *      ),
     *     @SWG\Property(
     *      property="password",
     *      example="12345",
     *      type="string",
     *      ),
     *     @SWG\Property(
     *      property="role",
     *      example=1,
     *      minProperties=0,
     *      maxProperties=8,
     *      type="integer",
     *      )
     *     )
     * ),
     *      @SWG\Response(
     *     response=201,
     *     description="Success"
     *      ),
     *     @SWG\Response(
     *     response="default",
     *     description="an ""unexpected"" error",
     *     @SWG\Schema(ref="#/definitions/User")
     *      )
     * ))
     *
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
        $username= $request->input('username');
        $email=$request->input('email');
        $role=$request->input('role');
        $password= Hash::make($request->input('password'));

        $register = User::create([
            'username'=>$username,
            'email'=>$email,
            'password'=>$password,
            'role'=>$role
        ]);

        if($register)
        {
            return response()->json([
                'success'=>true,
                'message'=>'Register Success',
                'data'=>$register
            ],201);
        }
        else{
            return response()->json([
                'success'=>false,
                'message'=>'Register Fail',
                'data'=>''
            ],400);
        }



    }

    /**
     * @SWG\Post(
     *     path="/login",
     *     deprecated=true,
     *     description="deprecated, see identitymanager to get access",
     *     operationId="login",
     *     summary="Login User",
     *     tags={"user"},
     *     @SWG\Parameter(
     *     name="User",
     *     in="body",
     *     description="User to create",
     *     @SWG\Schema(
     *     @SWG\Property(
     *     property="email",
     *     example="33216028@students.itb.ac.id",
     *     type="string",
     *      ),
     *     @SWG\Property(
     *     property="password",
     *     example="33216028",
     *     type="string",
     *      ),
     *      )
     * ),
     *     @SWG\Response(
     *     response=200,
     *     description="Success!"
     * ),
     *     @SWG\Response(
     *     response=403,
     *     description="The Username or Password not Valid!"
     * ),
     *
     *
     *
     * )
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $username= $request->input('email');
        $password= $request->input('password');

        $user = User::where('email', $username)->first();
        if(Hash::check($password,  optional($user)->password))
        {
            $apiToken = base64_encode(Str::random(40));
            $user->update(
                ['api_token'=>$apiToken]
            );
            {
                return response()->json([
                    'success'=>true,
                    'message'=>'Login Success',
                    'data'=>[
                        'user'=>$user,
                        'api_token'=>$apiToken
                    ]
                ],201);
            }
        }
        else{
            return response()->json([
                'success'=>false,
                'message'=>'Login Failed',
                'data'=>''
            ]);
        }

    }

}