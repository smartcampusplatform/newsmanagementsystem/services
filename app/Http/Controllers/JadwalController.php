<?php
/**
 * Created by PhpStorm.
 * User: Asus
 * Date: 29/04/2019
 * Time: 14:40
 */

namespace App\Http\Controllers;


use App\Jadwal;
use App\User;
use Illuminate\Http\Request;

class JadwalController extends Controller
{
    protected $holiday;
    public function __construct(CalendarController $calendar)
    {
        $this->middleware('auth',['except'=>['index']]);
        $this->holiday=$calendar;
    }

    /**
     *   @SWG\Get(
     *   path="/jadwal",
     *   operationId="index",
     *   summary="Get All Schedule Reporting in ITB",
     *   tags={"jadwal"},
     *   @SWG\Response(
     *     response=200,
     *     description="Working"
     *   ),
     *   @SWG\Response(
     *     response="default",
     *     description="an ""unexpected"" error",
     *   )
     * )
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $jadwal= Jadwal::all()->toArray();
        return response()->json(array_values($jadwal),200,[],JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);
    }

    /**
     * @SWG\Get(
     *     path="/jadwal/{id}/",
     *     operationId="show",
     *     summary="Get one schedule detail from specified ID",
     *     tags={"jadwal"},
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="ID kegiatan",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="u",
     *         in="query",
     *         description="Token SSO",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *     response="200",
     *     description="Returns some faculty web address",
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="Error: Bad request. When required parameters were not supplied.",
     *     ),
     * )
     */
    public function show($id)
    {
        $jadwal = Jadwal::find($id);
        return response()->json(array_values($jadwal),200,[],JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);
    }


    /**
     * @SWG\Post(
     *     path="/jadwal",
     *     operationId="store",
     *     tags={"jadwal"},
     *     summary="Store the item of schedule",
     *     @SWG\Parameter(
     *     in="body",
     *     name="body",
     *     description="The Schedule to create",
     *     @SWG\Schema(
     *     @SWG\Property(
     *      property="id_kegiatan",
     *     example=1,
     *     type="integer"
     *              ),
     *     @SWG\Property(
     *      property="tanggal",
     *     example="2019-04-30",
     *     type="string"
     *              ),
     *     @SWG\Property(
     *      property="status",
     *     example=0,
     *     type="integer"
     *              ),
     *          )
     *      ),
     *     @SWG\Parameter(
     *     in="query",
     *      name="u",
     *     type="string",
     * description="user token from SSO identity manager"
     * ),
     *     @SWG\Response(
     *     response=201,
     *     description="Success"
     *   ),
     *   @SWG\Response(
     *     response="default",
     *     description="an unexpected error"
     *   )
     *
     * )
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        if ($request->user())
        {
            $user = $request->user()->login_id;
            $reporter= User::whereUsername($user)->first()->id;
            $jadwal= new Jadwal();
            $jadwal->reporter= $reporter;
            $jadwal->id_kegiatan= $request->input('id_kegiatan');
            $jadwal->tanggal= $request->input('tanggal');
            $jadwal->status=1;

            if ($jadwal->save())
            {
                return response()->json(['message'=>'Sukses',
                    'data'=>$jadwal
                ],201);
            }

        }
        return response()->json(['message'=>'Only registered user',
        ],400,[],JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);





    }


}