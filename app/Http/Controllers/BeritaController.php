<?php
/**
 * Created by PhpStorm.
 * User: Asus
 * Date: 03/04/2019
 * Time: 20:02
 */

namespace App\Http\Controllers;

use App\Berita;
use App\Tag;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Swagger\Annotations as SWG;


class BeritaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index']]);
    }

    /**
     * @SWG\Get(
     *   path="/news",
     *   operationId="index",
     *   summary="Get All Published News from ITB web",
     *   tags={"news"},
     *   @SWG\Response(
     *     response=200,
     *     description="Working"
     *   ),
     *   @SWG\Response(
     *     response="default",
     *     description="an ""unexpected"" error",
     *   )
     * )
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $berita = Berita::publish()->get()->toArray();

        return response()->json(($berita), 200, [], JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
    }

    /**
     * @SWG\Get(
     *   path="/news/draft",
     *   operationId="draft",
     *   summary="Get All Draft Article News in ITB",
     *     description="in order to gain access, token must be as admin (0) or PR(3)",
     *   tags={"news"},
     *     @SWG\Parameter(
     *         name="u",
     *         in="query",
     *         description="Token SSO from IdentityManagement",
     *         required=true,
     *         type="string"
     *     ),
     *   @SWG\Response(
     *     response=200,
     *     description="Working"
     *   ),
     *     @SWG\Response(
     *     response=401,
     *     description="Error: Unauthorized"
     *   ),
     *   @SWG\Response(
     *     response="default",
     *     description="an ""unexpected"" error",
     *   )
     * )
     * @return \Illuminate\Http\JsonResponse
     */
    public function draft(Request $request)
    {
//        $user= $request->user();
//      $user =Auth::user();
//        dd($role);
//        if(!empty($user)&&$user->login_role==0)
        {
            $berita = Berita::draft()->get()->toArray();
            return response()->json(array_values($berita), 200, [], JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
        }
//        else{
//            return response()->json(['message'=>'Admin page only'],401,[],JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);
//        }

    }


    /**
     * @SWG\Post(
     *     path="/news",
     *     operationId="create",
     *     tags={"news"},
     *     summary="Create News Article include tags",
     *     @SWG\Parameter(
     *     in="body",
     *     name="body",
     *     description="The News Article  to create",
     *     @SWG\Schema(
     *     @SWG\Property(
     *      property="title",
     *     example="News ABCD",
     *     type="string"
     *              ),
     *     @SWG\Property(
     *      property="date",
     *     example="2019-04-30",
     *     type="string"
     *              ),
     *     @SWG\Property(
     *      property="content",
     *     example="This is example of the content of news article",
     *     type="string"
     *              ),
     *     @SWG\Property(
     *      property="tag",
     *     type="array",
     *     example={{"tag_name":"tag1"},{"tag_name":"tag2"}},
     *     @SWG\Items(
     *     type="string",
     *              )
     *             )
     *     ),
     *      ),
     *     @SWG\Parameter(
     *     in="query",
     *      name="u",
     *     type="string",
     * description="user token from SSO identity manager"
     * ),
     *     @SWG\Response(
     *     response=201,
     *     description="Success"
     *   ),
     *   @SWG\Response(
     *     response="default",
     *     description="an unexpected error"
     *   )
     *
     * )
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $tags = [];
        $author = null;
//        Ubah menjadi cek token di Identity Management
//        if ($request->header('Authorization')) {
//            $apiToken=explode(' ',$request->header('Authorization'));
//            $user= User::where('api_token', $apiToken[1])->first();
//            $author= $user->id;
//        }
        $user = $request->user()->login_id;
        if ($request->user())
        {
            $author= User::whereUsername($user)->first()->id;
            $berita = new Berita();
            $berita->author = $author;
            $berita->title = $request->input('title');
            $berita->content = $request->input('content');
            $berita->date = $request->input('date');
//          Coba input tag sekalian
            $input = $request->input('tag');
//           dd($input);
            if ($input != null) {
                foreach ($request->input('tag') as $tagsData) {
                    $tagNama = $tagsData['tag_name'];
                    $cek = Tag::ofTagName($tagNama)->first();
                    if (!empty($cek)) {
                        $tags[] = $cek;
                    } else {
                        $tags[] = new Tag($tagsData);
                    }
                }
            }
            if ($berita->save()) {
                iF (!empty($tags)) {
//                cek agar tidak duplikat tag dengan nama sama
                    $berita->tags()->saveMany(array_unique($tags));
//                $berita->syncChanges()
                }

                return response()->json(['message' => 'Sukses',
                    'data' => $berita], 201);
            } else
                return response()->json(['message' => 'Failed',
                ], 400);
        }
        else
            return response()->json(['message'=>'Only registered user',
            ],400,[],JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);

    }



    /**
     * @SWG\Get(
     *     path="/news/detail/{id}/",
     *     operationId="/news",
     *     summary="Get one news detail from specified ID",
     *     tags={"news"},
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="ID agenda",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="u",
     *         in="query",
     *         description="Token SSO",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *     response="200",
     *     description="Returns some faculty web address",
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="Error: Bad request. When required parameters were not supplied.",
     *     ),
     * )
     */
    public function show(Request $request,$id)
    {
        if ($request->user()) {
            $berita = Berita::find($id);
            return response()->json(json_decode($berita), 200, [], JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
        } else {
            return response()->json(['message' => 'Admin page only'], 401, [], JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
        }

    }


    /**
     * @SWG\Delete(
     *     path="/news/{id}/",
     *     operationId="/delete",
     *     summary="Delete one news detail from specified ID",
     *     tags={"news"},
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="ID agenda",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="u",
     *         in="query",
     *         description="Token SSO",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *     response="201",
     *     description="Resource Updated",
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="Error: Bad request. When required parameters were not supplied.",
     *     ),
     * )
     */
    public function delete($id)
    {
        $fakultas = Fakultas::find($id);
        $fakultas->update(['status' => 0]);
        return response()->json(['message' => 'Successfully delete faculty']);
    }


}