<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 *
 * @property int $id
 * @property int $id_berita
 * @property int $id_user
 * @property string $comment
 * @property boolean $hate_speech
 * @property string $confidence
 * @property string $created_at
 * @property string $updated_at
 * @property User $user
 * @property Berita $beritum
 *
 *
 *
 */

/**
 * @SWG\Definition()
 * Class Komentar
 * @package App
 */
class Komentar extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'komentar';

    /**
     * @var array
     */
    protected $fillable = ['id_berita', 'id_user', 'comment','hate_speech','confidence'];
    protected $appends=['user_name','berita_title'];
    protected $hidden=['id_user','user','beritum'];

    protected $casts=[
        'confidence'=>'float'
    ];


    /**
     * @param Builder $query
     */
    public function scopeHate($query){
        return $query->where('hate_speech','=',true);
    }

    /**
     * bukan kategori hatespeech tetapi confidence nya tidak sampai 0.7
     * @param Builder  $query
     */
    public function scopeConfidence($query)
    {
        return $query->where([['hate_speech','<>','true'],
            ['confidence','<', 0.70]]);
    }

    public function getUserNameAttribute($value)
    {
        $user_name=null;
        if($this->user)
        {
            $user_name=$this->user->username;
        }
        return $user_name;
    }

    public function getBeritatitleAttribute()
    {
        $berita_title=null;
        if($this->beritum)
        {
            $berita_title= $this->beritum->title;
        }
        return $berita_title;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'id_user');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function beritum()
    {
        return $this->belongsTo('App\Berita', 'id_berita');
    }
}


/**
 * @SWG\Property(property="id", type="integer", example=1)
 * @SWG\Property(property="id_berita", type="integer", example=1)
 * @SWG\Property(property="id_user", type="integer", example=1, description="userid dari pemberi komentar")
 * @SWG\Property(property="comment", type="string", example="Contoh Komentar", description="teks komentar")
 * @SWG\Property(property="hate_speech", type="boolean", example=false, default=null, description="hasil identifikasi hatespeech")
 * @SWG\Property(property="confidence", type="string", example="0.70", default=null, description="confidence hasil identifikasi hatespeech, rentang 0.0 sampai 1.0")
 * @SWG\Property(property="created_at", type="string", example="2019-04-22 01:38:59")
 * @SWG\Property(property="updated_at", type="string", example="2019-04-22 01:38:59")

 */
