<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Swagger\Annotations as SWG;

/**
 * @property int $id
 * @property string $username
 * @property string $email
 * @property string $password
 * @property string $api_token
 * @property integer $role
 * @property string $created_at
 * @property string $updated_at
 * @property Berita[] $beritas
 * @property Jadwal $jadwalReporter
 * @property Feedback $feedback
 * @property Agenda[] $agendas
 * @property Agenda[] $verifikators
 * @property Komentar[] $komentars
 */

/**
 * @SWG\Definition(
 * )
 * Class User
 * @package App
 */
class User extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'user';

    /**
     * @var array
     */
    protected $fillable = ['username', 'role', 'email','password','api_token'];

    protected  $hidden=['password','api_token','login_role'];

    protected $appends=['rolename'];


    public function getRolenameAttribute($value)
    {
        $rolename=null;
        switch ($this->role)
        {
            case 0: $rolename= "admin";
                break;
            case 1: $rolename='Mahasiswa';
                    break;
            case 2: $rolename='Dosen';
                    break;
            case 3: $rolename = 'Staf Teknis';
                    break;
            case 4: $rolename = 'Staf Akademik Kampus';
                break;
            case 5: $rolename = 'Tamu';
                break;
            case 6: $rolename = 'Dekan';
                break;
            case 7: $rolename = 'Kaprodi';
                break;
            case 8: $rolename = 'Staf Akademik Prodi';
                break;

        }

        return $rolename;

    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function beritas()
    {
        return $this->hasMany('App\Berita', 'author');
    }

    public function editors()
    {
        return $this->hasMany('App\Berita', 'modified_by');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function jadwalReporter()
    {
        return $this->hasOne('App\Jadwal', 'reporter');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function feedback()
    {
        return $this->hasOne('App\Feedback', 'id_user');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function agendas()
    {
        return $this->hasMany('App\Agenda', 'id_author');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function verfikators()
    {
        return $this->hasMany('App\Agenda', 'id_verificator');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function komentars()
    {
        return $this->hasMany('App\Komentar', 'id_user');
    }
    /**
     * @SWG\Property(property="id", type="integer", example=1)
     * @SWG\Property(property="userid", type="string", example="novianto")
     * @SWG\Property(property="email", type="string", example="novianto@s.itb.ac.id")
     * @SWG\Property(property="password", type="string", example="123456")
     * @SWG\Property(property="api_token", type="string")
     * @SWG\Property(property="role", type="integer", description="0 - Admin<br>1- Mahasiswa<br>2- Dosen<br>3- Staf Teknis<br>4- Staf Akademik Kampus<br>5- Tamu<br>6- Dekan<br>7- Kaprodi<br>8- Staf Akademik Prodi",
     *     minProperties=0, maxProperties=8)
     */

}
