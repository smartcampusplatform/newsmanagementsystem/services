<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $n_response
 * @property string $created_at
 * @property string $updated_at
 * @property User $user
 */
class Feedback extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'tr_kuesioner';

    /**
     * @var array
     */
    protected $fillable = ['id', 'n_response', 'created_at', 'updated_at'];

    public $timestamps = false;
}
