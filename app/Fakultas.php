<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Swagger\Annotations as SWG;

/**
 *
 * @property int $id
 * @property string $nama_fakultas
 * @property string $url_web
 * @property string $nama_tag
 * @property boolean $status
 * @property string $created_at
 * @property string $updated_at
 */


//TODO create Fakultas Schema

/**
 *  @SWG\Definition(
 *
 * )
 * Class Fakultas
 * @package App
 */
class Fakultas extends Model
{

    protected $table = 'fakultas';
//    timestamp
    /**
     * @var array
     */
    protected $fillable = ['nama_fakultas', 'url_web', 'nama_tag', 'status','lat','long'];


/**
 * @SWG\Property(property="id", type="integer", example=1)
 * @SWG\Property(property="nama_fakultas", type="string", example="Fakultas Teknik Mesin dan Dirgantara (FTMD)" )
 * @SWG\Property(property="nama_tag", type="string", example="berita", default="berita")
 * @SWG\Property(property="status", type="boolean", example=true, default=true)
 * @SWG\Property(property="created_at", type="string", example="2019-04-22 01:38:59")
 * @SWG\Property(property="updated_at", type="string", example="2019-04-22 01:38:59")
 * @SWG\Property(property="lat", type="string", example="-6.890631")
 * @SWG\Property(property="long", type="string", example="107.610543")
 */




}
