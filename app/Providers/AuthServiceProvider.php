<?php

namespace App\Providers;

use App\User;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot()
    {
        // Here you may define how you wish users to be authenticated for your Lumen
        // application. The callback which receives the incoming request instance
        // should return either a User instance or null. You're free to obtain
        // the User instance via an API token or any other method necessary.

        $this->app['auth']->viaRequest('api', function (Request $request) {
            if ($request->query('u')) {
                $token=$request->query('u');
//                $apiToken=explode(' ',$request->header('Authorization'));
                $client = new Client([
                    'verify'=>false
                ]);

                $res = $client->get("http://178.128.104.74/identitymanagement/api/token/".$token);
                $code= $res->getStatusCode();
                if($code==200)
                {
                    $body= json_decode($res->getBody()->getContents());

//                dd($body->data);
                    if(!empty($body->data))
                    {
                        $data = $body->data;
                        return $data;

                    }
                }
            }

        });
    }
}
