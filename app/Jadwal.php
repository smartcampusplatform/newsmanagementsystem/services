<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $id_kegiatan
 * @property int $reporter
 * @property string $tanggal
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 * @property User $user
 * @property Kegiatan $kegiatan
 * @property Peliputan $peliputan
 */

/**
 * @SWG\Definition()
 * Class Jadwal
 * @package App
 */
class Jadwal extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'jadwal_reporter';

    /**
     * @var array
     */
    protected $fillable = ['id_kegiatan', 'reporter','tanggal', 'status'];
    protected $appends=['status_name'];

    public function getStatusNameAttribute($value)
    {
        $status_name= null;
        if($this->status==0)
            $status_name="Dijadwalkan";
        elseif ($this->status==1)
            $status_name= "Selesai";
        elseif ($this->status==2)
            $status_name= "Tunda";
        else
            $status_name="Dibatalkan";
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'reporter');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function kegiatan()
    {
        return $this->belongsTo('App\Kegiatan', 'id_kegiatan');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function peliputan()
    {
        return $this->hasOne('App\Peliputan', 'id_jadwal');
    }

    /**
     * @SWG\Property(property="id", type="integer", example=1)
     * @SWG\Property(property="id_kegiatan", type="integer", example=1)
     * @SWG\Property(property="reporter", type="integer", example=1, description="userid reporter")
     * @SWG\Property(property="tanggal", type="string", example="2019-04-30")
     * @SWG\Property(property="status", type="string", example=1, default=0, description="0 - Dijadwalkan <br>1 - Selesai<br>2 - Ditunda")
     * @SWG\Property(property="created_at", type="string", example="2019-04-22 01:38:59")
     * @SWG\Property(property="updated_at", type="string", example="2019-04-22 01:38:59")
     */
}
