<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Swagger\Annotations as SWG;

/**
 * @property int $id
 * @property int $id_author
 * @property int $status
 * @property int $id_verificator
 * @property string $nama_agenda
 * @property string $tanggal
 * @property string $organizer
 * @property string $category_event
 * @property string $partisipan
 * @property string $link_agenda
 * @property string $deskripsi
 * @property string $created_at
 * @property string $updated_at
 * @property User $user
 * @property User $verificator
 */

/**
 * @SWG\Definition(
 * )
 * Class Agenda
 * @package App
 */
class Agenda extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'agenda';

    /**
     * @var array
     */
    protected $fillable = ['id_author', 'nama_agenda', 'tanggal', 'organizer', 'category_event', 'partisipan', 'link_agenda', 'deskripsi','status','id_verificator'];
    protected $hidden=['id_author','user','verificator','id_verificator'];
    protected $appends=['author_name','verificator_name'];

    public function getAuthorNameAttribute($value)
    {
        $author_name=null;
        if($this->user)
        {
            $author_name=$this->user->username;
        }
        return $author_name;
    }
    public function getVerificatorNameAttribute($value)
    {
        $verificator=null;
        if($this->verificator)
        {
            $verificator=$this->verificator->username;
        }
        return $verificator;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'id_author');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function verificator()
    {
        return $this->belongsTo('App\User', 'id_verificator');
    }

    /**
     * @param Builder $query
     */
    public function scopeDraft($query)
    {
        return $query->where('status','=',0);

    }
    /**
     * @param Builder $query
     */
    public function scopePublish($query)
    {
        return $query->where('status','=',1);

    }

    /**
     * @SWG\Property(property="id", type="integer", example=1)
     * @SWG\Property(property="id_author", type="integer", example=1, description="userid who created agendas")
     * @SWG\Property(property="id_verificator", type="integer", example=1, description="userid who created agendas")
     * @SWG\Property(property="tanggal", type="string", example="2019-04-30")
     * @SWG\Property(property="organizer", type="string", example="Fakultas ABC")
     * @SWG\Property(property="category_event", type="string", example="International / National")
     * @SWG\Property(property="partisipan", type="string", example="Mahasiswa/Umum")
     * @SWG\Property(property="link_agenda", type="string", example="http://example.oom")
     * @SWG\Property(property="deskripsi", type="string", example="deskripsi agenda")
     * @SWG\Property(property="status", type="integer", example=0, enum={0,1})
     * @SWG\Property(property="created_at", type="string", example="2019-04-22 01:38:59")
     * @SWG\Property(property="updated_at", type="string", example="2019-04-22 01:38:59")
     */
}
