<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $id_jadwal
 * @property int $hasil_peliputan
 * @property string $kendala
 * @property string $created_at
 * @property string $updated_at
 * @property Jadwal $jadwalReporter
 */

/**
 * @SWG\Definition()
 * Class Peliputan
 * @package App
 */
class Peliputan extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'peliputan';

    /**
     * @var array
     */
    protected $fillable = ['id_jadwal', 'hasil_peliputan', 'kendala'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function jadwalReporter()
    {
        return $this->belongsTo('App\Jadwal', 'id_jadwal');
    }

    /**
     * @SWG\Property(property="id", type="integer", example=1)
     * @SWG\Property(property="id_jadwal", type="integer", example=1)
     * @SWG\Property(property="hasil_peliputan", type="integer", example=1, description="")
     * @SWG\Property(property="kendala", type="string", example="Detail kendala", description="kendala yang dialami reporter")
     * @SWG\Property(property="created_at", type="string", example="2019-04-22 01:38:59")
     * @SWG\Property(property="updated_at", type="string", example="2019-04-22 01:38:59")
     */
}
