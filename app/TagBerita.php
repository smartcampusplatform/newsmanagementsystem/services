<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $tag_id
 * @property int $berita_id
 * @property Tag $tag
 * @property Berita $beritum
 */
class TagBerita extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'berita_tag';

    /**
     * @var array
     */
    protected $fillable = ['tag_id', 'berita_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tag()
    {
        return $this->belongsTo('App\Tag', 'tag_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function beritum()
    {
        return $this->belongsTo('App\Berita', 'berita_id');
    }
}
