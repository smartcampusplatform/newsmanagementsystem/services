<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
//    return $router->app->version();
    return redirect()->to('/api/documentation');
});


//Generate Application Key
$router->get('/key', function (){
    return \Illuminate\Support\Str::random(32);
});
//Welcome using GET method
//$router->get('/',function (){
//   return 'News Management API powered by Lumen using GET Method';
//});

//Welcome using POST method
$router->post('/',function (){
    return 'News Management API powered by Lumen using POST Method';
});

$router->group(['prefix' => 'api',], function () use ($router)
{
//  USER
    $router->post('/register','AuthController@register');
    $router->post('/login','AuthController@login');


//  FAKULTAS
    $router->get('faculties', 'FakultasController@index');
    $router->get('faculties/{id}', 'FakultasController@show');
    $router->post('faculties', 'FakultasController@store');
    $router->put('/faculties/{id}', 'FakultasController@update');
    $router->delete('faculties/{id}', 'FakultasController@delete');

//  $router->get('grab','GrabNewsController@grab');

//  GRAB NEWS dan ARTIKEL
    $router->get('scrap','GrabNewsController@scrap');
    $router->get('scrap/{id}','GrabNewsController@news');
    $router->get('scrap/{id}/detail/{id_post}','GrabNewsController@postId'); //digunakan untuk melihat detail post
    $router->get('flash/{id1}/{id2}','GrabNewsController@getDistance'); //digunakan untuk melihat detail post
    $router->get('jarak/{id1}/{id2}','GrabNewsController@jarakFakultas'); //digunakan untuk melihat detail post
    $router->get('/nearby','GrabNewsController@nearbyFakultas');


//    TODO ARTIKEL BERITA

    //    Feedback
    $router->get('feedback','FeedbackController@index');
    $router->post('feedback','FeedbackController@create');

//    Kuesioner
    $router->get('kuesioner','KuesionerController@index');
    $router->get('kuesioner/{id}','KuesionerController@show');
    $router->post('kuesioner','KuesionerController@create');
    $router->put('kuesioner/{id}','KuesionerController@update');
    $router->delete('kuesioner/{id}', 'KuesionerController@delete');
//    test runner

//    AGENDA
    $router->get('agenda','AgendaController@index');
    $router->get('agenda/detail/{id}','AgendaController@show');
    $router->get('agenda/draft','AgendaController@draft');
    $router->post('agenda','AgendaController@create');
    $router->post('agenda/verify/{id}','AgendaController@verify');

//Berita sekaligus Tag nya
    $router->get('news/draft','BeritaController@draft');
    $router->get('news','BeritaController@index');
    $router->post('/news','BeritaController@store');
    $router->get('news/{id}','BeritaController@show');
    $router->delete('/news/{id}','BeritaController@delete');

//Jadwal
    $router->get('/jadwal/holiday/{year}','CalendarController@holiday');
    $router->get('/jadwal','JadwalController@index');
    $router->post('/jadwal','JadwalController@store');
    $router->get('/jadwal/{id}','JadwalController@show');

//    KOMENTAR
    $router->get('news/{id}/comments','KomentarController@show');
    $router->get('comments','KomentarController@index');   //ok
    $router->post('/comments','KomentarController@store');  //ok
    $router->post('/comments/hatespeech','KomentarController@hate');  //ok
    $router->get('comments/hatespeech','KomentarController@getHate');

//  Kegiatan
    $router->get('kegiatan','KegiatanController@index');
    $router->get('kegiatan/{id}','KegiatanController@show');
    $router->post('/kegiatan','KegiatanController@store');


    //Generate Application Key
    $router->get('/key', function (){
        return \Illuminate\Support\Str::random(32);
    });
//Welcome using GET method
    $router->get('/',function (){
//        return 'News Management API powered by Lumen using GET Method';
//        return redirect()->to('/api/documentation');
        return redirect()->to('/api/documentation');
    });

//Welcome using POST method
    $router->post('/',function (){
        return 'News Management API powered by Lumen using POST Method';
    });

});