<?php

function getConnection() {
  $db_host = getenv('DB_HOST');
  $db_user = getenv('DB_USER');
  $db_pass = getenv('DB_PASS');
  $db_name = getenv('DB_NAME');
  $dbh = new PDO("pgsql:host=$db_host;dbname=$db_name", $db_user, $db_pass);
  $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  return $dbh;
}
